package com.prinmode.cineapp.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import com.prinmode.cineapp.exceptions.ResourceNotFoundException;
import com.prinmode.cineapp.model.dto.UsuarioDTO;
import com.prinmode.cineapp.model.entities.Cliente;
import com.prinmode.cineapp.model.entities.Rol;
import com.prinmode.cineapp.model.entities.Usuario;
import com.prinmode.cineapp.model.services.IClienteService;
import com.prinmode.cineapp.model.services.IRolService;
import com.prinmode.cineapp.model.services.IUsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class UsuarioController {

  @Autowired
  private IUsuarioService service;

  @Autowired
  private IRolService rolService;

  @Autowired
  private IClienteService clienteService;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  @Secured({ "ROLE_ADMIN", "ROLE_USER" })
  @GetMapping("/usuarios")
  public ResponseEntity<List<Usuario>> list() {
    List<Usuario> lista = this.service.list();

    return new ResponseEntity<List<Usuario>>(lista, HttpStatus.OK);
  }

  @Secured({ "ROLE_ADMIN", "ROLE_USER" })
  @GetMapping("/usuarios/{id}")
  public ResponseEntity<Usuario> getbyId(@PathVariable("id") Integer id) {
    Usuario entity = this.service.getById(id);
    if (entity == null) {
      throw new ResourceNotFoundException(String.format("Usuario %s no encontrado", id.toString()));
    }
    return new ResponseEntity<Usuario>(entity, HttpStatus.OK);
  }

  @Secured({ "ROLE_ADMIN", "ROLE_USER" })
  @PostMapping("/usuarios")
  public ResponseEntity<Object> registrar(@Valid @RequestBody UsuarioDTO dto) {

    // TODO: if !(dto validator) => throw exception!!

    String clave = passwordEncoder.encode(dto.getClave());

    Rol rol = this.rolService.findByNombre(dto.getRol());
    Cliente cliente = new Cliente();
    cliente.setNombres(dto.getNombre());
    cliente.setApellidos(dto.getApellidos());
    cliente.setDni(dto.getDni());
    cliente.setFechaNac(dto.getFechanac());
    cliente = this.clienteService.insert(cliente);

    Usuario usuario = new Usuario();
    usuario.setUsername(dto.getUsername());
    usuario.setClave(clave);
    usuario.setEnabled(true);
    usuario.setCliente(cliente);
    usuario.addRoles(rol);

    usuario = this.service.insert(usuario);

    cliente.setUsuario(usuario);
    this.clienteService.update(cliente);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/usuarios/{id}")
        .buildAndExpand(usuario.getIdUsuario()).toUri(); // redirect to /api/usuario/{id}
    return ResponseEntity.created(location).build();
  }

}