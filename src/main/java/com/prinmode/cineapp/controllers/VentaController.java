package com.prinmode.cineapp.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import com.prinmode.cineapp.exceptions.ResourceNotFoundException;
import com.prinmode.cineapp.model.entities.Venta;
import com.prinmode.cineapp.model.services.IVentaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class VentaController {

  @Autowired
  private IVentaService service;

  @GetMapping
  public ResponseEntity<List<Venta>> list() {
    List<Venta> lista = this.service.list();

    return new ResponseEntity<List<Venta>>(lista, HttpStatus.OK);
  }

  @GetMapping("/ventas/{id}")
  public ResponseEntity<Venta> getbyId(@PathVariable("id") Integer id) {
    Venta entity = this.service.getById(id);
    if (entity == null) {
      throw new ResourceNotFoundException("ID NO ENCONTRADO " + id);
    }
    return new ResponseEntity<Venta>(entity, HttpStatus.OK);
  }

  @PostMapping("/ventas")
  public ResponseEntity<Object> registrar(@Valid @RequestBody Venta entity) {
    entity = this.service.insert(entity);

    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(entity.getIdVenta())
        .toUri();
    return ResponseEntity.created(location).build();
  }

}