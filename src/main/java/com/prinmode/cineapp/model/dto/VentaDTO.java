package com.prinmode.cineapp.model.dto;

import com.prinmode.cineapp.model.entities.Venta;

import java.util.List;

import com.prinmode.cineapp.model.entities.Comida;


public class VentaDTO {

  private Venta venta;

  private List<Comida> comidas;

  public VentaDTO() {
    super();
  }

  public Venta getVenta() {
    return venta;
  }

  public void setVenta(Venta venta) {
    this.venta = venta;
  }

  public List<Comida> getComidas() {
    return comidas;
  }

  public void setComidas(List<Comida> comidas) {
    this.comidas = comidas;
  }

  public void addComida(Comida comida){
    this.comidas.add(comida);
  }

  public void removeComida(Comida comida){
    this.comidas.remove(comida);
  }
}
