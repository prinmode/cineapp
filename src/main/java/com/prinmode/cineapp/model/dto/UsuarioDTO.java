package com.prinmode.cineapp.model.dto;

import java.time.LocalDate;

public class UsuarioDTO {

  private String dni;
  private LocalDate fechanac;
  private String nombre;
  private String apellidos;
  private String username;
  private String clave;
  private String rol;

  public UsuarioDTO() {
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellidos() {
    return apellidos;
  }

  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getClave() {
    return clave;
  }

  public void setClave(String clave) {
    this.clave = clave;
  }

  public String getRol() {
    return rol;
  }

  public void setRol(String rol) {
    this.rol = rol;
  }

  public String getDni() {
    return dni;
  }

  public void setDni(String dni) {
    this.dni = dni;
  }

  public LocalDate getFechanac() {
    return fechanac;
  }

  public void setFechanac(LocalDate fechanac) {
    this.fechanac = fechanac;
  }

  public UsuarioDTO(String dni, LocalDate fechanac, String nombre, String apellidos, String username, String clave,
      String rol) {
    this.dni = dni;
    this.fechanac = fechanac;
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.username = username;
    this.clave = clave;
    this.rol = rol;
  }

}