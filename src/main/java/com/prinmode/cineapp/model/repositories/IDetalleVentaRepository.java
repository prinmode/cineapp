package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.DetalleVenta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDetalleVentaRepository extends JpaRepository<DetalleVenta, Integer> {

}