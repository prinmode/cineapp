package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Integer> {
  @Query("select u from Usuario u where u.username=?1")
  public Usuario findByUsername(String username);
}