package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Venta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IVentaRepository extends JpaRepository<Venta, Integer> {

}