package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Rol;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IRolRepository extends JpaRepository<Rol, Integer> {
  @Query("SELECT r FROM Rol r WHERE r.nombre = ?1")
  public Rol findByNombre(String nombre);
}