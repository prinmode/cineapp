package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Config;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IConfigRepository extends JpaRepository<Config, Integer> {

  @Query("select c from Config c where c.parametro=?1")
  Config FindByParametro(String parametro);
}