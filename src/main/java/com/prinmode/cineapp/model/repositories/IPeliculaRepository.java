package com.prinmode.cineapp.model.repositories;


import com.prinmode.cineapp.model.entities.Pelicula;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPeliculaRepository extends JpaRepository<Pelicula, Integer> {

}