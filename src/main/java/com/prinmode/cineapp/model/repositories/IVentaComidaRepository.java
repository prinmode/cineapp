package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.VentaComida;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IVentaComidaRepository extends JpaRepository<VentaComida, Integer> {

  @Modifying
  @Query(value = "INSERTO INTO ventas_comidas(id_venta, id_comida) VALUES (:idVenta, :idComida)", nativeQuery = true)
  int Registrar(@Param("idVenta") Integer idVenta, @Param("idComida") Integer idComida);

}