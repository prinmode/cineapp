package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Comida;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IComidaRepository extends JpaRepository<Comida, Integer> {

}