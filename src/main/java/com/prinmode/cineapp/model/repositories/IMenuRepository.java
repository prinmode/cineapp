package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Menu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMenuRepository extends JpaRepository<Menu, Integer> {

}