package com.prinmode.cineapp.model.repositories;

import com.prinmode.cineapp.model.entities.Genero;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IGeneroRepository extends JpaRepository<Genero, Integer> {

}