package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Pelicula;

public interface IPeliculaService extends ICrud<Pelicula> {
}