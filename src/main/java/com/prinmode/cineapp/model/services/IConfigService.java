package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Config;

public interface IConfigService extends ICrud<Config> {
}