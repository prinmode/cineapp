package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Genero;

public interface IGeneroService extends ICrud<Genero> {
}