package com.prinmode.cineapp.model.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.prinmode.cineapp.model.entities.Usuario;
import com.prinmode.cineapp.model.repositories.IUsuarioRepository;
import com.prinmode.cineapp.model.services.IUsuarioService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService implements IUsuarioService, UserDetailsService {

  private Logger logger = LoggerFactory.getLogger(UsuarioService.class);

  @Autowired
  private IUsuarioRepository repository;

  @Override
  @Transactional
  public Usuario insert(Usuario entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional
  public Usuario update(Usuario entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Usuario> list() {
    return this.repository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Usuario getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

  @Override
  @Transactional(readOnly = true)
  public Usuario findByUsername(String username) {
    return this.repository.findByUsername(username);
  }

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    Usuario usuario = this.repository.findByUsername(username);

    if (usuario == null) {
      logger.error("Error en el login: no existe el usuario '" + username + "' en el sistema!");
      throw new UsernameNotFoundException("Error en el login: no existe el usuario '" + username + "' en el sistema!");
    }

    List<GrantedAuthority> authorities = usuario.getRoles().stream()
        .map(role -> new SimpleGrantedAuthority(role.getNombre()))
        .peek(authority -> logger.info("Role: " + authority.getAuthority())).collect(Collectors.toList());

    return new User(usuario.getUsername(), usuario.getClave(), usuario.isEnabled(), true, true, true, authorities);
  }

}