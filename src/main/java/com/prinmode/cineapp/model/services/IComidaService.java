package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Comida;

public interface IComidaService extends ICrud<Comida> {
}