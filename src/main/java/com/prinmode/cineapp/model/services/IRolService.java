package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Rol;

public interface IRolService extends ICrud<Rol> {
  Rol findByNombre(String nombre);
}