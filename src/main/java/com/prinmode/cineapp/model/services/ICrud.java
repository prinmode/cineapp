package com.prinmode.cineapp.model.services;

import java.util.List;

public interface ICrud<T> {
  T insert(T entity);
  T update(T entity);
  List<T> list();
  T getById(Integer id);
  boolean remove(Integer id);
} 