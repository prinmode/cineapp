package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Menu;

public interface IMenuService extends ICrud<Menu> {
}