package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.entities.Pelicula;
import com.prinmode.cineapp.model.repositories.IPeliculaRepository;
import com.prinmode.cineapp.model.services.IPeliculaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeliculaService implements IPeliculaService {

  @Autowired
  private IPeliculaRepository repository;

  @Override
  public Pelicula insert(Pelicula entity) {
    return this.repository.save(entity);
  }

  @Override
  public Pelicula update(Pelicula entity) {
    return this.repository.save(entity);
  }

  @Override
  public List<Pelicula> list() {
    return this.repository.findAll();
  }

  @Override
  public Pelicula getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

}