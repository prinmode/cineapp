package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.entities.Comida;
import com.prinmode.cineapp.model.repositories.IComidaRepository;
import com.prinmode.cineapp.model.services.IComidaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComidaService implements IComidaService {

  @Autowired
  private IComidaRepository repository;

  @Override
  public Comida insert(Comida entity) {
    return this.repository.save(entity);
  }

  @Override
  public Comida update(Comida entity) {
    return this.repository.save(entity);
  }

  @Override
  public List<Comida> list() {
    return this.repository.findAll();
  }

  @Override
  public Comida getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

}