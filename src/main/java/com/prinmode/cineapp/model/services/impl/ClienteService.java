package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.entities.Cliente;
import com.prinmode.cineapp.model.repositories.IClienteRepository;
import com.prinmode.cineapp.model.services.IClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteService implements IClienteService {

  @Autowired
  private IClienteRepository repository;

  @Override
  @Transactional
  public Cliente insert(Cliente entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional
  public Cliente update(Cliente entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Cliente> list() {
    return this.repository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Cliente getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

}