package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.entities.Genero;
import com.prinmode.cineapp.model.repositories.IGeneroRepository;
import com.prinmode.cineapp.model.services.IGeneroService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneroService implements IGeneroService {

  @Autowired
  private IGeneroRepository repository;

  @Override
  public Genero insert(Genero entity) {
    return this.repository.save(entity);
  }

  @Override
  public Genero update(Genero entity) {
    return this.repository.save(entity);
  }

  @Override
  public List<Genero> list() {
    return this.repository.findAll();
  }

  @Override
  public Genero getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

}