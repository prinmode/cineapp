package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.dto.VentaDTO;
import com.prinmode.cineapp.model.entities.Venta;
import com.prinmode.cineapp.model.repositories.IVentaComidaRepository;
import com.prinmode.cineapp.model.repositories.IVentaRepository;
import com.prinmode.cineapp.model.services.IVentaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VentaService implements IVentaService {

  @Autowired
  private IVentaRepository repository;

  @Autowired
  private IVentaComidaRepository ventaComidaRepository;

  @Override
  @Transactional
  public Venta insert(Venta entity) {
    entity.getDetalleVentas().forEach(item -> {
      item.setVenta(entity);
    });
    return this.repository.save(entity);
  }

  @Override
  @Transactional
  public Venta update(Venta entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Venta> list() {
    return this.repository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Venta getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

  @Override
  @Transactional
  public Venta RegistrarTransactional(VentaDTO dto) {
    // Relaciona los detalles de venta entrantes con la entidad Venta
    dto.getVenta().getDetalleVentas().forEach(item -> {
      item.setVenta(dto.getVenta());
    });

    this.repository.save(dto.getVenta());

    // Genera los registros VentaComida a partir de las entidades Comida y venta del
    // DTO
    dto.getComidas().forEach(item -> {
      this.ventaComidaRepository.Registrar(dto.getVenta().getIdVenta(), item.getIdComida());
    });

    return dto.getVenta();
  }

}