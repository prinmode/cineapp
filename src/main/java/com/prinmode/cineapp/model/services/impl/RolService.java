package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.entities.Rol;
import com.prinmode.cineapp.model.repositories.IRolRepository;
import com.prinmode.cineapp.model.services.IRolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RolService implements IRolService {

  @Autowired
  private IRolRepository repository;

  @Override
  @Transactional
  public Rol insert(Rol entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional
  public Rol update(Rol entity) {
    return this.repository.save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Rol> list() {
    return this.repository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Rol getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

  @Override
  @Transactional(readOnly = true)
  public Rol findByNombre(String nombre) {
    return this.repository.findByNombre(nombre);
  }

}