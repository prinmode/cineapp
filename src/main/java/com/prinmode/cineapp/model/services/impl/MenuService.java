package com.prinmode.cineapp.model.services.impl;

import java.util.List;

import com.prinmode.cineapp.model.entities.Menu;
import com.prinmode.cineapp.model.repositories.IMenuRepository;
import com.prinmode.cineapp.model.services.IMenuService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuService implements IMenuService {

  @Autowired
  private IMenuRepository repository;

  @Override
  public Menu insert(Menu entity) {
    return this.repository.save(entity);
  }

  @Override
  public Menu update(Menu entity) {
    return this.repository.save(entity);
  }

  @Override
  public List<Menu> list() {
    return this.repository.findAll();
  }

  @Override
  public Menu getById(Integer id) {
    return this.repository.findById(id).orElse(null);
  }

  @Override
  public boolean remove(Integer id) {
    this.repository.deleteById(id);
    return this.getById(id) == null;
  }

}