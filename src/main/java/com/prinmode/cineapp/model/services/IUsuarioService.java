package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Usuario;

public interface IUsuarioService extends ICrud<Usuario> {
  Usuario findByUsername(String username);
}