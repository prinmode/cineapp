package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.dto.VentaDTO;
import com.prinmode.cineapp.model.entities.Venta;

public interface IVentaService extends ICrud<Venta> {
  Venta RegistrarTransactional(VentaDTO dto);
}