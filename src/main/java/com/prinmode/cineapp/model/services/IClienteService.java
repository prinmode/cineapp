package com.prinmode.cineapp.model.services;

import com.prinmode.cineapp.model.entities.Cliente;

public interface IClienteService extends ICrud<Cliente> {
}