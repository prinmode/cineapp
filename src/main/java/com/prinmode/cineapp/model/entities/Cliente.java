package com.prinmode.cineapp.model.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idCliente;

  @Column(nullable = false, length = 80)
  private String nombres;

  @Column(nullable = false, length = 80)
  private String apellidos;

  @Column(nullable = false)
  private LocalDate fechaNac;

  @Column(nullable = false, length = 8)
  private String dni;

  // relations
  // onetoone usuario
  @OneToOne(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  private Usuario usuario;

  @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Venta> ventas;

  public Cliente() {
    this.ventas = new ArrayList<>();
  }

  public int getIdCliente() {
    return this.idCliente;
  }

  public void setIdCliente(int idCliente) {
    this.idCliente = idCliente;
  }

  public String getNombres() {
    return this.nombres;
  }

  public void setNombres(String nombres) {
    this.nombres = nombres;
  }

  public String getApellidos() {
    return this.apellidos;
  }

  public void setApellidos(String apellidos) {
    this.apellidos = apellidos;
  }

  public LocalDate getFechaNac() {
    return this.fechaNac;
  }

  public void setFechaNac(LocalDate fechaNac) {
    this.fechaNac = fechaNac;
  }

  public String getDni() {
    return this.dni;
  }

  public void setDni(String dni) {
    this.dni = dni;
  }

  public Usuario getUsuario() {
    return this.usuario;
  }

  public void setUsuario(Usuario usuario) {
    usuario.setCliente(this);
    this.usuario = usuario;
  }

  public void removeUsuario() {
    if (this.usuario != null) {
      this.usuario.setCliente(null);
      this.usuario = null;
    }
  }

  public List<Venta> getVentas() {
    return this.ventas;
  }

  public void setVentas(List<Venta> ventas) {
    this.ventas = ventas;
  }

  public void addVenta(Venta venta) {
    this.ventas.add(venta);
    venta.setCliente(this);
  }

  public void removeVenta(Venta venta) {
    this.ventas.remove(venta);
    venta.setCliente(null);
  }

}