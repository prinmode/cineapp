package com.prinmode.cineapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "config")
public class Config {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idConfig;

  @Column(nullable = false, length = 5)
  private String parametro;

  @Column(length = 25)
  private String valor;

  public Config() {
    super();
  }

  public int getIdConfig() {
    return idConfig;
  }

  public void setIdConfig(int idConfig) {
    this.idConfig = idConfig;
  }

  public String getParametro() {
    return parametro;
  }

  public void setParametro(String parametro) {
    this.parametro = parametro;
  }

  public String getValor() {
    return valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }

}