package com.prinmode.cineapp.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "genero")
public class Genero {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idGenero;

  @Column(nullable = false, length = 20)
  private String nombre;

  @OneToMany(mappedBy = "genero", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Pelicula> peliculas;

  public Genero() {
    super();
  }

  public int getIdGenero() {
    return idGenero;
  }

  public void setIdGenero(int idGenero) {
    this.idGenero = idGenero;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public List<Pelicula> getPeliculas() {
    return peliculas;
  }

  public void setPeliculas(List<Pelicula> peliculas) {
    this.peliculas = peliculas;
  }

  public void addPelicula(Pelicula pelicula) {
    pelicula.setGenero(this);
    this.peliculas.add(pelicula);
  }

  public void removePelicula(Pelicula pelicula) {
    this.peliculas.remove(pelicula);
    pelicula.setGenero(null);
  }

}