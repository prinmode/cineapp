package com.prinmode.cineapp.model.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "peliculas")
public class Pelicula {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idPelicula;

  @Column(nullable = false, length = 255)
  private String nombre;

  @Column(nullable = false, length = 255)
  private String resena;

  @Column(nullable = false, length = 3)
  private short duracion;

  @Column(nullable = false)
  private LocalDate fechaPublicacion;

  @Column(nullable = false, length = 255)
  private String urlPortada;

  // relations
  // manytoone genero

  @ManyToOne
  @JoinColumn(name = "id_genero", nullable = false, foreignKey = @ForeignKey(name = "FK_cliente_usuario"))
  private Genero genero;

  @OneToMany(mappedBy = "pelicula", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Venta> ventas;

  public Pelicula() {
    super();
  }

  public int getIdPelicula() {
    return idPelicula;
  }

  public void setIdPelicula(int idPelicula) {
    this.idPelicula = idPelicula;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getResena() {
    return resena;
  }

  public void setResena(String resena) {
    this.resena = resena;
  }

  public short getDuracion() {
    return duracion;
  }

  public void setDuracion(short duracion) {
    this.duracion = duracion;
  }

  public LocalDate getFechaPublicacion() {
    return fechaPublicacion;
  }

  public void setFechaPublicacion(LocalDate fechaPublicacion) {
    this.fechaPublicacion = fechaPublicacion;
  }

  public String getUrlPortada() {
    return urlPortada;
  }

  public void setUrlPortada(String urlPortada) {
    this.urlPortada = urlPortada;
  }

  public Genero getGenero() {
    return genero;
  }

  public void setGenero(Genero genero) {
    this.genero = genero;
  }

  public List<Venta> getVentas() {
    return ventas;
  }

  public void setVentas(List<Venta> ventas) {
    this.ventas = ventas;
  }

  public void addVenta(Venta venta) {
    venta.setPelicula(this);
    this.ventas.add(venta);
  }

  public void removeVenta(Venta venta) {
    venta.setPelicula(null);
    this.ventas.remove(venta);
  }
}