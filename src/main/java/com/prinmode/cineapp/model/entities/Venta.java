package com.prinmode.cineapp.model.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "ventas")
public class Venta {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idVenta;

  private LocalDate fecha;

  @Column(length = 2)
  private int cantidad;

  private double total;

  // relations
  // manyToOne cliente
  // manytoone pelicula,
  // onetomany detalleventa
  @ManyToOne(fetch = FetchType.EAGER)
  private Cliente cliente;

  @ManyToOne
  @JoinColumn(name = "id_pelicula", nullable = false, foreignKey = @ForeignKey(name = "FK_venta_pelicula"))
  private Pelicula pelicula;

  @OneToMany(mappedBy = "venta", cascade = { CascadeType.ALL }, orphanRemoval = true)
  private List<DetalleVenta> detalleVentas;

  public Venta() {
    super();
  }

  public int getIdVenta() {
    return idVenta;
  }

  public void setIdVenta(int idVenta) {
    this.idVenta = idVenta;
  }

  public LocalDate getFecha() {
    return fecha;
  }

  public void setFecha(LocalDate fecha) {
    this.fecha = fecha;
  }

  public int getCantidad() {
    return cantidad;
  }

  public void setCantidad(int cantidad) {
    this.cantidad = cantidad;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public Cliente getCliente() {
    return this.cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }

  public Pelicula getPelicula() {
    return pelicula;
  }

  public void setPelicula(Pelicula pelicula) {
    this.pelicula = pelicula;
  }

  public List<DetalleVenta> getDetalleVentas() {
    return detalleVentas;
  }

  public void setDetalleVentas(List<DetalleVenta> detalleVentas) {
    this.detalleVentas = detalleVentas;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + idVenta;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Venta other = (Venta) obj;
    if (idVenta != other.idVenta)
      return false;
    return true;
  }

}