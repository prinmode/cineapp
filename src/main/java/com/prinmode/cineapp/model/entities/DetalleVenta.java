package com.prinmode.cineapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.ForeignKey;

@Entity
@Table(name = "detallesventas")
public class DetalleVenta {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idDetalleVenta;

  @Column(nullable = false, length = 3)
  private String asiento;

  // relations
  // ManyToOne Venta

  @ManyToOne
  @JoinColumn(name = "id_venta", nullable = false, foreignKey = @ForeignKey(name = "FK_detalleventa_venta"))
  private Venta venta;

  public DetalleVenta() {
    super();
  }

  public int getIdDetalleVenta() {
    return idDetalleVenta;
  }

  public void setIdDetalleVenta(int idDetalleVenta) {
    this.idDetalleVenta = idDetalleVenta;
  }

  public String getAsiento() {
    return asiento;
  }

  public void setAsiento(String asiento) {
    this.asiento = asiento;
  }

  public Venta getVenta() {
    return venta;
  }

  public void setVenta(Venta venta) {
    this.venta = venta;
  }

}