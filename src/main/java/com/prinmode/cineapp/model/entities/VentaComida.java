package com.prinmode.cineapp.model.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "ventas_comidas")
@IdClass(VentaComidaPK.class)
public class VentaComida {

  @Id
  private Venta venta;
  @Id
  private Comida comida;

  public VentaComida() {
    super();
  }

  public Venta getVenta() {
    return venta;
  }

  public void setVenta(Venta venta) {
    this.venta = venta;
  }

  public Comida getComida() {
    return comida;
  }

  public void setComida(Comida comida) {
    this.comida = comida;
  }

}