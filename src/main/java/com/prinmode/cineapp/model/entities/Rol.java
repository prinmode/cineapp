package com.prinmode.cineapp.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Rol implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idRol;

  @Column(nullable = false)
  private String nombre;

  private String descripcion;

  @ManyToMany(cascade = { CascadeType.ALL, CascadeType.MERGE })
  private List<Menu> menues;

  public Rol() {
    super();
    this.menues = new ArrayList<>();
  }

  public int getIdRol() {
    return idRol;
  }

  public void setIdRol(int idRol) {
    this.idRol = idRol;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public List<Menu> getMenues() {
    return menues;
  }

  public void setMenues(List<Menu> menues) {
    this.menues = menues;
  }

  public void addMenu(Menu menu) {
    this.menues.add(menu);
  }

  public void removeMenu(Menu menu) {
    this.menues.remove(menu);
  }

}