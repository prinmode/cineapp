package com.prinmode.cineapp.model.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "menues")
public class Menu {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int idMenu;

  @Column(length = 20)
  private String icono;

  @Column(length = 20)
  private String nombre;

  @Column(length = 50)
  private String url;

  // relations
  // manytomany roles

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "menu_rol", joinColumns = @JoinColumn(name = "id_menu", referencedColumnName = "idMenu"), inverseJoinColumns = @JoinColumn(name = "id_rol", referencedColumnName = "idRol"))
  private List<Rol> roles;

  public Menu() {
    super();
  }

  public int getIdMenu() {
    return idMenu;
  }

  public void setIdMenu(int idMenu) {
    this.idMenu = idMenu;
  }

  public String getIcono() {
    return icono;
  }

  public void setIcono(String icono) {
    this.icono = icono;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<Rol> getRoles() {
    return roles;
  }

  public void setRoles(List<Rol> roles) {
    this.roles = roles;
  }

}