package com.prinmode.cineapp.config;

import java.util.HashMap;
import java.util.Map;

import com.prinmode.cineapp.model.entities.Usuario;
import com.prinmode.cineapp.model.services.IUsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;

@Component
public class CustomTokenEnhacer implements TokenEnhancer {

  @Autowired
  private IUsuarioService usuarioService;

  @Override
  public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

    Usuario usuario = usuarioService.findByUsername(authentication.getName());

    // Cliente cliente = usuario.getCliente() != null ? usuario.getCliente() : new
    // Cliente();

    // custom token details
    Map<String, Object> info = new HashMap<>();
    info.put("username", usuario.getUsername());
    // info.put("nombre", cliente.getNombres());
    // info.put("apellidos", cliente.getApellidos());
    // info.put("dni", cliente.getDni());

    ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);

    return accessToken;
  }

}